import scrapy


class WestElmPricesSpider(scrapy.Spider):
    name = 'west_elm_prices'
    allowed_domains = ['www.westelm.com']
    start_urls = ['http://www.westelm.com/shop/rugs/all-rugs/?cm_sp=supercat/']

    def parse(self, response):
        pass
